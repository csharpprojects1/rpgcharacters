﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    // Enum for different weapon types
    public enum WeaponType
    {
        Axe, Sword, Hammer, Dagger, Bow, Staff, Wand
    }
    // Class for weapon items
    public class Weapon : Items
    {
        // Weapon Properties
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponStats { get; set; }

    }
}
