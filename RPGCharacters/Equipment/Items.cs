﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
	// Enum for each equipment slot for the character.
	public enum Slots
    {
		Head, Body, Leg, Weapon
	}
	// Main class for Equipment
	public class Items
	{
		// Item propeties
		public string ItemName { get; set; }
		public int ItemLevel { get; set; }
		// Makes the enum public
        public Slots ItemSlot { get; set; }

	}

}
