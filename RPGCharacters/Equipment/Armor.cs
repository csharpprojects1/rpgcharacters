﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPGCharacters
{
    // Enum for armor types
    public enum ArmorType
    {
        Cloth, Leather, Mail, Plate
    }

    // Class for armor items
    public class Armor : Items
    {
        // Armor propeties
        public ArmorType ArmorType { get; set; }
        public PrimaryAttributes ArmorStats { get; set; }
    }

}
