﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Game
    {
        Character character;
        Combat combat = new();
        StringBuilder sb = new();

        /// <summary>
        /// Starts the application and gives an introduction to the user.
        /// </summary>
        public void StartGame()
        {

            Console.WriteLine("Press any Key to Begin Your Adventure!");
            Console.ReadKey();
            PickClass();
            CreateCharacter(GetClassInput(), GetNameInput());
            DisplayCharacter(character);
            GetOptionInput();

        }

        /// <summary>
        /// Method for naming the character.
        /// The user has to input at least 1 letter else it will continue to loop.
        /// </summary>
        /// <returns>
        /// Returns the name that user has given and applies that to the character.
        /// </returns>
        public string GetNameInput()
        {
            Console.WriteLine("Name your Hero!");
            string Name = "";
            while(Name.Length == 0)
            {
                Console.Clear();
                Console.WriteLine("Name your Hero!");
                Name = Console.ReadLine();
            }
            Console.Clear();
            return Name;
        }

        /// <summary>
        /// Method for picking class to the character.
        /// The user has to make an input between 1 and 4 in order to pick a class.
        /// Else it will continue to loop.
        /// </summary>
        /// <returns>
        /// Returns the input value from the user and takes it to the selected option.
        /// </returns>
        public int GetClassInput()
        {
            var ChooseClass = Console.ReadLine();
            int Choice;
            while (!int.TryParse(ChooseClass, out Choice) || Choice < 1 || Choice > 4)
            {
                Console.Clear();
                ErrorMessage(1, 4);
                PickClass();
                ChooseClass = Console.ReadLine();
            }
            Console.Clear();
            return Choice;
        }



        /// <summary>
        /// Method that takes input between 1 and 4 in order to execute another method.
        /// </summary>
        /// <returns>
        /// Returns the input value from the user and takes it to the selected option.
        /// </returns>
        public int GetOptionInput()
        {
            var InputChoice = Console.ReadLine();
            int CheckInput;
            while (!int.TryParse(InputChoice, out CheckInput) || CheckInput < 1 || CheckInput > 4)
            {
                Console.Clear();
                ErrorMessage(1, 4);
                ChooseAction(ActionChoices());
                InputChoice = Console.ReadLine();
            }

            Console.Clear();
            return CheckInput;
        }

        // Shows the user what classes there is to pick between.
        public void PickClass()
        {
            Console.Clear();
            Console.WriteLine("Pick a Class!");
            sb.Append("1. Mage" + "\n" + "2. Ranger" + "\n" + "3. Rogue" + "\n" + "4. Warrior");
            Console.WriteLine(sb);
            sb.Clear();
        }

        /// <summary>
        /// Reads the input from GetOptionInput method with help of an while loop. 
        /// So if the value is not between 1 and 4 it will ask the user to do another input.
        /// </summary>
        /// <returns>
        /// Returns the value based from what the user choice to do.
        /// </returns>
        public int ActionChoices()
        {
            sb.Append("1. Level up" + "\n" + "2. Show stats" + "\n" + "3. Fight for Weapon" + "\n" + "4. Fight for Armor");
            Console.WriteLine(sb);
            var OptionChoice = Console.ReadLine();
            int Choice;
            while (!int.TryParse(OptionChoice, out Choice) || Choice < 1 || Choice > 4)
            {
                Console.Clear();
                Console.WriteLine(sb);
                OptionChoice = Console.ReadLine();
            }
            sb.Clear();
            return Choice;

        }

        // Method to display what the user should enter
        public static void ErrorMessage(int from, int to)
        {
            Console.WriteLine($"\nPlease enter a number between {from} and {to}");
        }

        /// <summary>
        /// Method to create the character which takes in an input from the user which is between 1 and 4.
        /// Takes in additional string Input which is the name for the character.
        /// </summary>
        /// <param name="choice"></param>
        /// <param name="name"></param>
        public void CreateCharacter(int choice, string name)
        {
            switch (choice)
            {
                case 1:
                    character = new Mage(name);
                    break;
                case 2:
                    character = new Ranger(name);
                    break;
                case 3:
                    character = new Rogue(name);
                    break;
                case 4:
                    character = new Warrior(name);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Method that calls on CalcDPS which calculates the current total DPS.
        /// After the calculations is done it will then display the current stats on the character.
        /// </summary>
        /// <param name="character"></param>
        public void DisplayCharacter(Character character)
        {
            character.CalcDps();
            sb.Append("Name: " + character.Name + "\n" + "Level: " + character.Level + "\n" + "Strength: " + character.BaseAttributes.Strength + "\n" + "Dexterity: " + character.BaseAttributes.Dexterity + "\n" + "Intelligence: " + character.BaseAttributes.Intelligence + "\n" + "Damage: " + character.HeroDps);
            Console.WriteLine(sb);
            sb.Clear();
        }

        /// <summary>
        /// Method to call the CreateRandomWeapon if the random roll is 1 else it will say that you need to try again.
        /// If successfully it will then ask if you want to try and equip the weapon.
        /// If your character are able to it will show a message that tells its been equipped.
        /// Else it will throw an exeption that tells you a reason why you can't equip it.
        /// </summary>
        public void FightForWeapon()
        {
            Random rdn = new Random();
            int a = rdn.Next(0, 2);
            if (a == 1)
            {
                Console.WriteLine("You won!");
                combat.CreateRandomWeapon();
                Console.WriteLine("Try Equip? y/n");
                string answer = Console.ReadLine();
                try
                {
                    if (answer.ToLower() == "y")
                    {
                        Console.WriteLine(character.EquipWeapon(combat.GetWeapon()));
                    }
                    if(answer.ToLower() == "n")
                    {
                        Console.WriteLine("You leave the weapon behind.");
                    }

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }
            else
                Console.WriteLine("You lost! Try Again!");
        }

        /// <summary>
        /// Method to call the CreateRandomArmor if the random roll is 1 else it will say that you need to try again.
        /// If successfully it will then ask if you want to try and equip the Armor piece.
        /// If your character are able to it will show a message that tells its been equipped.
        /// Else it will throw an exeption that tells you a reason why you can't equip it.
        /// </summary>
        public void FightForArmor()
        {
            Random rdn = new Random();
            int a = rdn.Next(0, 2);
            if (a == 1)
            {
                Console.WriteLine("You won!");
                combat.CreateRandomArmor();
                Console.WriteLine("Try Equip? y/n");
                string answer = Console.ReadLine();
                try
                {
                    if (answer.ToLower() == "y")
                    {
                        Console.WriteLine(character.EquipArmor(combat.GetArmor()));
                    }
                    if (answer.ToLower() == "n")
                    {
                        Console.WriteLine("You leave the armorpiece behind.");
                    }

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }
            else
                Console.WriteLine("You lost! Try Again!");
        }

        /// <summary>
        /// A switch method that takes the input from the user and calls on the selected method.
        /// </summary>
        /// <param name="a"></param>
        public void ChooseAction(int a)
        {
            switch (a)
            {
                case 1:
                    Console.WriteLine("You have gained a Level! You are now Level: " + character.Levelup(1));
                    break;
                case 2:
                    DisplayCharacter(character);
                    break;
                case 3:
                    FightForWeapon();
                    break;
                case 4:
                    FightForArmor();
                    break;
                default:
                    break;
            }
        }
    }
}
