﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{

    public class Combat : Items
    {
        // Random used to roll the weapons and armor.
        readonly Random rnd = new Random();
        Weapon newWeapon;
        Armor newArmor;

        /// <summary>
        /// Creates an random weapon with random stats and type. The item name is based on what weapontype it becomes.
        /// </summary>
        /// <returns>
        /// Returns a random generated weapon and also a string which includes information about the weapon.
        /// </returns>
        public Weapon CreateRandomWeapon()
        {
            int Ilevel = rnd.Next(1, 4);
            int DmgRoll = rnd.Next(2, 42);
            int AtkSpdRollModifier = rnd.Next(1, 4);
            double AtkSpdRoll = rnd.NextDouble();
            int RandomWpnType = rnd.Next(0, 6);
            string[] wpns = new string[] { "Axe", "Sword", "Hammer", "Dagger", "Bow", "Staff", "Wand" };
            Weapon weapon = new Weapon()
            {
                ItemName = wpns[RandomWpnType],
                ItemLevel = Ilevel,
                ItemSlot = Slots.Weapon,
                WeaponType = (WeaponType)RandomWpnType,
                WeaponStats = new WeaponAttributes() { Damage = DmgRoll, AttackSpeed = Math.Round(AtkSpdRoll) + AtkSpdRollModifier }
            };
            StringBuilder sb = new();
            sb.Append("Dropped: " + weapon.ItemName + "\n" + "Type: " + weapon.WeaponType + "\n" + "Level: " + weapon.ItemLevel + "\n" + "Damage: " + weapon.WeaponStats.Damage + "\n" + "AttackSpeed: " + weapon.WeaponStats.AttackSpeed);
            Console.WriteLine(sb);
            newWeapon = weapon;
            return newWeapon;
        }

        /// <summary>
        /// Creates an random armor with random stats and type. The item name is based on what armortype it becomes,
        /// </summary>
        /// <returns>
        /// Returns a random generated armor and also a string which includes information about the armor.
        /// It also contains an if which checks the armortype to roll the correct stats for that type. And that overrides the first stat roll.
        /// </returns>
        public Armor CreateRandomArmor()
        {
            string[] arms = new string[] { "Cloth", "Leather", "Mail", "Plate" };
            int roll = rnd.Next(0, 3);
            int statRoll = rnd.Next(1, 5);
            int lowerStatRoll = rnd.Next(1, 3);
            int Ilevel = rnd.Next(1, 4);
            int RandomArmType = rnd.Next(arms.Length);
            Armor armor = new Armor()
            {
                ItemName = arms[RandomArmType],
                ItemLevel = Ilevel,
                ItemSlot = (Slots)roll,
                ArmorType = (ArmorType)RandomArmType,
                ArmorStats = new PrimaryAttributes() { Strength = statRoll, Dexterity = statRoll, Intelligence = statRoll }
            };
            newArmor = armor;
            if(RandomArmType == 0)
            {
                newArmor.ArmorStats = new() { Intelligence = statRoll };
            }
            if(RandomArmType == 1)
            {
                newArmor.ArmorStats = new() { Dexterity = statRoll };
            }
            if(RandomArmType == 2)
            {
                newArmor.ArmorStats = new() { Strength = lowerStatRoll, Dexterity = lowerStatRoll };
            }
            if(RandomArmType == 3)
            {
                newArmor.ArmorStats = new() { Strength = statRoll };
            }
            StringBuilder sb = new();
            sb.Append("Dropped: " + armor.ItemName + "\n" + "Type: " + armor.ArmorType + "\n" + "Level: " + armor.ItemLevel + "\n" + "Equipment Slot: " + armor.ItemSlot + "\n" + "Strength: " + armor.ArmorStats.Strength + "\n" + "Dexterity: " + armor.ArmorStats.Dexterity + "\n" + "Intelligence: " + armor.ArmorStats.Intelligence);
            Console.WriteLine(sb);
            return newArmor;
        }

        // Gets and return the randomized armor
        public Armor GetArmor()
        {
            Armor armor = newArmor;
            return armor;
        }

        // Gets and return the randomized weapon.
        public Weapon GetWeapon()
        {
            Weapon weapon = newWeapon;
            return weapon;
        }

    }
}
