﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    class ExceptionWeapon : Exception
    {
        // Exception used if user tries to equip wrong weapon.
        public ExceptionWeapon()
        {
        }

        public ExceptionWeapon(string message) : base(message)
        {
        }

    }
}
