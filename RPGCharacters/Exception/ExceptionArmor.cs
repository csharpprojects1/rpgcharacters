﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    class ExceptionArmor : Exception
    {
        // Exception used if user tries to equip wrong armor.
        public ExceptionArmor()
        {
        }

        public ExceptionArmor(string message) : base(message)
        {
        }

    }
}
