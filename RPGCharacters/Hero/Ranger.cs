﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPGCharacters
{
    public class Ranger : Character
    {
        // Ranger base stats
        public Ranger(string name) : base(name, 1, 7, 1)
        {

        }

        /// <summary>
        /// Checks if Ranger is able to equip a certain weapon which should only be Bow.
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>
        /// Equips if valid weapon else throws an exeption that gives an explanation that you can't equip the weapon 
        /// if its not the correct type or if its to high level compared to the character.
        /// </returns>
        public override string EquipWeapon(Weapon weapon)
        {
            if (weapon.WeaponType != WeaponType.Bow)
            {
                throw new ExceptionWeapon($"You can't use this weapon type!, {weapon.WeaponType}");
            }
            if (weapon.ItemLevel > Level)
            {
                throw new ExceptionWeapon($"Its to high level!, {weapon.ItemLevel}");
            }
            Equipment[Slots.Weapon] = weapon;
            CalcDps();
            return "You have successfully equipped a new Weapon!";
        }

        /// <summary>
        /// Checks if the Ranger is able to equip certain armor pieces which should only be Leather or Mail.
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>
        /// Equips if valid armor else throws an expetion that gives an explanation why you can't equip the armor piece
        /// If its not the correct type or if its to high level compared to the character.
        /// </returns>
        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmorType != ArmorType.Leather && armor.ArmorType != ArmorType.Mail)
            {
                throw new ExceptionArmor($"You can't wear this armor type!, {armor.ArmorType}");
            }
            if (armor.ItemLevel > Level)
            {
                throw new ExceptionArmor($"It's to high level!, {armor.ItemLevel}");
            }
            if (armor.ItemSlot == Slots.Head)
            {
                CheckCurrentHeadGear();
                Equipment[Slots.Head] = armor;
                CalculateHeadPiece();
                return "You have successfully equipped a new Head piece!";
            }
            else if (armor.ItemSlot == Slots.Body)
            {
                CheckCurrentBodyGear();
                Equipment[Slots.Body] = armor;
                CalculateBodyPiece();
                return "You have successfully equipped a new Body piece!";
            }

            CheckCurrentLegGear();
            Equipment[Slots.Leg] = armor;
            CalculateLegPiece();
            return "You have successfully equipped a new Leg piece!";

        }

        // Method for leveling up an Ranger, applying the correct stats
        public override int Levelup(int level)
        {
            TotalAttributes = new PrimaryAttributes() { Strength = 1, Dexterity = 5, Intelligence = 1 };
            BaseAttributes += TotalAttributes;
            Level += 1 * level;
            return Level;

        }

        // Method to calculate the current DPS
        public override double CalcDps()
        {
            double weaponDPS = CalculateTotalWeapon();
            HeroDps = weaponDPS * (1 + BaseAttributes.Dexterity / 100);
            return weaponDPS * HeroDps;
        }
    }
}
