﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPGCharacters
{
    public class Rogue : Character
    {
        int count = 2;
        // Rogue base stats
        public Rogue(string name) : base(name, 2, 6, 1)
        {

        }

        /// <summary>
        /// Checks if Rogue is able to equip a certain weapon which should only be Dagger or Sword.
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>
        /// Equips if valid weapon else throws an exeption that gives an explanation why you can't equip the weapon 
        /// if its not the correct type or if its to high level compared to the character.
        /// </returns>
        public override string EquipWeapon(Weapon weapon)
        {
            if (weapon.WeaponType != WeaponType.Dagger && weapon.WeaponType != WeaponType.Sword)
            {
                throw new ExceptionWeapon("You can't use this type of Weapon!");
            }
            if (weapon.ItemLevel > Level)
            {
                throw new ExceptionWeapon("It's to high Level!");
            }
            Equipment[Slots.Weapon] = weapon;
            CalcDps();
            return "You have successfully equipped a new Weapon!";
        }

        /// <summary>
        /// Checks if the Rogue is able to equip certain armor pieces which should only be Leather or Mail.
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>
        /// Equips if valid armor else throws an expetion that gives an explanation why you can't equip the armor piece
        /// If its not the correct type or if its to high level compared to the character.
        /// </returns>
        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmorType != ArmorType.Leather && armor.ArmorType != ArmorType.Mail)
            {
                throw new ExceptionArmor($"You can't wear this armor type!, {armor.ArmorType}");
            }
            if (armor.ItemLevel > Level)
            {
                throw new ExceptionArmor($"It's to high level!, {armor.ItemLevel}");
            }
            if (armor.ItemSlot == Slots.Head)
            {
                CheckCurrentHeadGear();
                Equipment[Slots.Head] = armor;
                CalculateHeadPiece();
                return "You have successfully equipped a new Head piece!";
            }
            else if (armor.ItemSlot == Slots.Body)
            {
                CheckCurrentBodyGear();
                Equipment[Slots.Body] = armor;
                CalculateBodyPiece();
                return "You have successfully equipped a new Body piece!";
            }

            CheckCurrentLegGear();
            Equipment[Slots.Leg] = armor;
            CalculateLegPiece();
            return "You have successfully equipped a new Leg piece!";

        }

        // Method for leveling up an Rogue, applying the correct stats
        public override int Levelup(int level)
        {
            TotalAttributes = new PrimaryAttributes() { Strength = 1, Dexterity = 4, Intelligence = 1 };
            BaseAttributes += TotalAttributes;
            Level += 1 * level;
            count++;
            return Level;
        }

        // Method to calculate the current DPS
        public override double CalcDps()
        {
            double weaponDPS = CalculateTotalWeapon();
            HeroDps = weaponDPS * (1 + BaseAttributes.Dexterity / 100);
            return weaponDPS * HeroDps;
        }
    }
}
