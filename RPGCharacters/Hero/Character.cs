﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public abstract class Character
    {
        // Propeties for the character
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BaseAttributes { get; set; }
        public PrimaryAttributes TotalAttributes { get; set; }
        public double HeroDps { get; set; }
        public Dictionary<Slots, Items> Equipment { get; set; }
        public WeaponAttributes WpnAttributes { get; set; }

        // Creates the template for each classes
        public Character(string name, int strength, int dexterity, int intelligence)
        {
            Name = name;
            Level = 1;
            BaseAttributes = new PrimaryAttributes() { Strength = strength, Dexterity = dexterity, Intelligence = intelligence };
            Equipment = new Dictionary<Slots, Items>();
        }

        // Abstract methods for each class to level up, equip new weapons or armor and calculate their DPS
        public abstract int Levelup(int level);
        public abstract string EquipWeapon(Weapon weapon);
        public abstract string EquipArmor(Armor armor);
        public abstract double CalcDps();

        /// <summary>
        /// Calculates stats from the Head gear piece by checking if its equipped and adds it to the total attributes of the character.
        /// </summary>
        /// <returns>
        /// Returns the added stats from the calculated Head gear piece.
        /// </returns>
        public PrimaryAttributes CalculateHeadPiece()
        {
            TotalAttributes = new PrimaryAttributes() { Strength = 0, Dexterity = 0, Intelligence = 0 };

            bool HeadEquipped = Equipment.TryGetValue(Slots.Head, out Items HeadPiece);
            if (HeadEquipped)
            {
                Armor arm = (Armor)HeadPiece;
                TotalAttributes += new PrimaryAttributes() { Strength = arm.ArmorStats.Strength, Dexterity = arm.ArmorStats.Dexterity, Intelligence = arm.ArmorStats.Intelligence };
            }
            
            BaseAttributes += TotalAttributes;
            return BaseAttributes;
        }

        /// <summary>
        /// Checks the current Head piece and removes the stat added to BaseAttributes as it being removed so should its stat increasement as well.
        /// But it will only do so if you try to equip a new Head piece.
        /// </summary>
        /// <returns>
        /// Returns the BaseAttributes to its value it was before the Head piece had been equipped.
        /// </returns>
        public PrimaryAttributes CheckCurrentHeadGear()
        {

            bool HeadEquipped = Equipment.TryGetValue(Slots.Head, out Items HeadPiece);
            if (HeadEquipped)
            {
                Armor arm = (Armor)HeadPiece;
                int statStr = arm.ArmorStats.Strength * 2;
                int statDex = arm.ArmorStats.Dexterity * 2;
                int statInt = arm.ArmorStats.Intelligence * 2;
                BaseAttributes += new PrimaryAttributes() { Strength = arm.ArmorStats.Strength - statStr, Dexterity = arm.ArmorStats.Dexterity - statDex, Intelligence = arm.ArmorStats.Intelligence - statInt };
            }

            return BaseAttributes;
        }

        /// <summary>
        /// Calculates stats from the Body gear piece by checking if its equipped and adds it to the total attributes of the character.
        /// </summary>
        /// <returns>
        /// Returns the added stats from the calculated Body gear piece.
        /// </returns>
        public PrimaryAttributes CalculateBodyPiece()
        {
            TotalAttributes = new PrimaryAttributes() { Strength = 0, Dexterity = 0, Intelligence = 0 };

            bool BodyEquipped = Equipment.TryGetValue(Slots.Body, out Items BodyPiece);
            if (BodyEquipped)
            {
                Armor arm = (Armor)BodyPiece;
                TotalAttributes += new PrimaryAttributes() { Strength = arm.ArmorStats.Strength, Dexterity = arm.ArmorStats.Dexterity, Intelligence = arm.ArmorStats.Intelligence };
            }
            BaseAttributes += TotalAttributes;
            return BaseAttributes;
        }

        /// <summary>
        /// Checks the current Body piece and removes the stat added to BaseAttributes as it being removed so should its stat increasement as well.
        /// But it will only do so if you try to equip a new Body piece.
        /// </summary>
        /// <returns>
        /// Returns the BaseAttributes to its value it was before the Body piece had been equipped.
        /// </returns>
        public PrimaryAttributes CheckCurrentBodyGear()
        {

            bool BodyEquipped = Equipment.TryGetValue(Slots.Body, out Items BodyPiece);
            if (BodyEquipped)
            {
                Armor arm = (Armor)BodyPiece;
                int statStr = arm.ArmorStats.Strength * 2;
                int statDex = arm.ArmorStats.Dexterity * 2;
                int statInt = arm.ArmorStats.Intelligence * 2;
                BaseAttributes += new PrimaryAttributes() { Strength = arm.ArmorStats.Strength - statStr, Dexterity = arm.ArmorStats.Dexterity - statDex, Intelligence = arm.ArmorStats.Intelligence - statInt };
            }

            return BaseAttributes;
        }

        /// <summary>
        /// Calculates stats from the Leg gear piece by checking if its equipped and adds it to the total attributes of the character.
        /// </summary>
        /// <returns>
        /// Returns the added stats from the calculated Leg gear piece.
        /// </returns>
        public PrimaryAttributes CalculateLegPiece()
        {
            TotalAttributes = new PrimaryAttributes() { Strength = 0, Dexterity = 0, Intelligence = 0 };

            bool LegEquipped = Equipment.TryGetValue(Slots.Body, out Items LegPiece);
            if (LegEquipped)
            {
                Armor arm = (Armor)LegPiece;
                TotalAttributes += new PrimaryAttributes() { Strength = arm.ArmorStats.Strength, Dexterity = arm.ArmorStats.Dexterity, Intelligence = arm.ArmorStats.Intelligence };
            }
            BaseAttributes += TotalAttributes;
            return BaseAttributes;
        }

        /// <summary>
        /// Checks the current Leg piece and removes the stat added to BaseAttributes as it being removed so should its stat increasement as well.
        /// But it will only do so if you try to equip a new Leg piece.
        /// </summary>
        /// <returns>
        /// Returns the BaseAttributes to its value it was before the Leg piece had been equipped.
        /// </returns>
        public PrimaryAttributes CheckCurrentLegGear()
        {

            bool LegEquipped = Equipment.TryGetValue(Slots.Body, out Items LegPiece);
            if (LegEquipped)
            {
                Armor arm = (Armor)LegPiece;
                int statStr = arm.ArmorStats.Strength * 2;
                int statDex = arm.ArmorStats.Dexterity * 2;
                int statInt = arm.ArmorStats.Intelligence * 2;
                BaseAttributes += new PrimaryAttributes() { Strength = arm.ArmorStats.Strength - statStr, Dexterity = arm.ArmorStats.Dexterity - statDex, Intelligence = arm.ArmorStats.Intelligence - statInt };
            }

            return BaseAttributes;
        }

        /// <summary>
        /// Checks if an weapon is equipped and calculates the total DPS.
        /// </summary>
        /// <returns>
        /// Returns the total DPS but if no weapon is equipped it will return 1 as base damage.
        /// </returns>
        public double CalculateTotalWeapon()
        {
            bool WpnEquipped = Equipment.TryGetValue(Slots.Weapon, out Items WpnPiece);
            if (WpnEquipped)
            {
                Weapon wpn = (Weapon)WpnPiece;
                return wpn.WeaponStats.Damage * wpn.WeaponStats.AttackSpeed;
            }
            else
            {
                return 1;
            }
        }

    }

}
