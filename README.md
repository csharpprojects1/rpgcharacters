# Getting Started

git clone https://gitlab.com/csharpprojects1/rpgcharacters.git

# Usage
When you start the program it will tell you what you need to do in order to progress.
First you just press any key to continue. After you pick a class and gives it a name.
When you have created your character you will be showed some options on what you can do.
Try it out and see what happens :)

### There is 4 different classes:

* Mage
* Ranger
* Rogue
* Warrior

### And each have an primary attribute:

* Strength
* Dexterity
* Intelligence

You can also aquire weapons and armor.

# Made by Anders Hansen-Haug
