using RPGCharacters;
using Xunit;


namespace RPGCharacterTest
{
    public class CharacterItemTest
    {
        Character newChar;

        [Fact]
        public void EquipWeapon_CheckIfAbleToEquipHigherLevelWeapon_ShouldThrowErrorCauseToLowLevel()
        {
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = Slots.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponStats = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            newChar = new Warrior("Ungabunga");
            newChar.EquipWeapon(testAxe);
        }

        [Fact]
        public void EquipArmor_CheckIfAbleToEquipHigherLevelArmor_ShouldThrowErrorCauseToLowLevel()
        {
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = Slots.Body,
                ArmorType = ArmorType.Plate,
                ArmorStats = new PrimaryAttributes() { Strength = 1 }
            };
            newChar = new Warrior("Ungabunga");
            newChar.EquipArmor(testPlateBody);
        }

        [Fact]
        public void EquipWeapon_CheckIfWarriorCanEquipBow_ShouldThrowErrorCauseWrongWeaponType()
        {
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slots.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponStats = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };
            newChar = new Warrior("Ungabunga");
            newChar.EquipWeapon(testBow);
        }

        [Fact]
        public void EquipArmor_CheckIfWarriorCanEquipCloth_ShouldThrowErrorCauseWrongArmorType()
        {
            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = Slots.Head,
                ArmorType = ArmorType.Cloth,
                ArmorStats = new PrimaryAttributes() { Intelligence = 5 }
            };
            newChar = new Warrior("Ungabunga");
            newChar.EquipArmor(testClothHead);
        }

        [Fact]
        public void EquipWeapon_CheckIfWarriorCanEquipWeapon_ShouldWorkAsItemLevelAndTypeIsCorrect()
        {
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slots.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponStats = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            newChar = new Warrior("Ungabunga");
            string expected = "You have successfully equipped a new Weapon!";
            string actual = newChar.EquipWeapon(testAxe);
            newChar.EquipWeapon(testAxe);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_CheckIfWarriorCanEquipArmor_ShouldWorkAsItemLevelAndTypeIsCorrect()
        {
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slots.Body,
                ArmorType = ArmorType.Plate,
                ArmorStats = new PrimaryAttributes() { Strength = 1 }
            };
            newChar = new Warrior("Ungabunga");
            string expected = "You have successfully equipped a new Body piece!";
            string actual = newChar.EquipArmor(testPlateBody);
            newChar.EquipArmor(testPlateBody);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalcDps_CheckIfValueIsCorrectWithoutWeapon_ShouldReturnDPSEqualsOne()
        {
            newChar = new Warrior("Ungabunga");
            newChar.CalcDps();
            double expected = 1 * (1 + (5 / 100));
            double actual = newChar.CalcDps();


            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalcDps_CheckIfVauleIsCorrectWithWeapon_ShouldReturnDPSEqualToWeaponStats()
        {
            newChar = new Warrior("Ungabunga");
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slots.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponStats = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            newChar.EquipWeapon(testAxe);
            newChar.CalcDps();
            double expected = (7 * 1.1) * (1 + (5 / 100));
            double actual = newChar.CalcDps();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWeaponArmorCalcDps_CheckIfValueIsCorrectWithWeaponAndArmor_ShouldReturnDPSEqualToWeaponAndArmorStats()
        {
            newChar = new Warrior("Ungabunga");
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slots.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponStats = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slots.Body,
                ArmorType = ArmorType.Plate,
                ArmorStats = new PrimaryAttributes() { Strength = 1 }
            };
            newChar.EquipWeapon(testAxe);
            newChar.EquipArmor(testPlateBody);
            newChar.CalculateBodyPiece();

            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            double actual = newChar.CalcDps();

            Assert.Equal(expected, actual);
        }
    }
}
