﻿using Xunit;
using RPGCharacters;

namespace RPGCharacterTest
{
   public class CharacterTest
    {
        [Fact]
        public void CreateCharacterMethod_CheckLevel_ShouldBeOne()
        {
            Mage mage = new Mage("Mage");
            int expected = mage.Level = 1;
            int actual = mage.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpMethod_CheckLevel_ShouldBeTwo()
        {
            Mage mage = new Mage("Mage");
            int expected = 2;
            int actual = mage.Levelup(1);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterWarrior_CheckBaseStats_ShouldBeStarterStats()
        {
            Warrior warrior = new Warrior("warrior");
            PrimaryAttributes expected = new() { Strength = 5, Dexterity = 2, Intelligence = 1 };
            PrimaryAttributes actual = warrior.BaseAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterRogue_CheckBaseStats_ShouldBeStarterStats()
        {
            Rogue rogue = new Rogue("rogue");
            PrimaryAttributes expected = new() { Strength = 2, Dexterity = 6, Intelligence = 1 };
            PrimaryAttributes actual = rogue.BaseAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterRanger_CheckBaseStats_ShouldBeStarterStats()
        {
            Ranger ranger = new Ranger("ranger");
            PrimaryAttributes expected = new() { Strength = 1, Dexterity = 7, Intelligence = 1 };
            PrimaryAttributes actual = ranger.BaseAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterMage_CheckBaseStats_ShouldBeStarterStats()
        {
            Mage mage = new Mage("mage");
            PrimaryAttributes expected = new() { Strength = 1, Dexterity = 1, Intelligence = 8 };
            PrimaryAttributes actual = mage.BaseAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterLevelUpWarrior_CheckLevelStats_ShouldBeAnStatIncrease()
        {
            Warrior warrior = new Warrior("warrior");
            PrimaryAttributes expected = new() { Strength = 8, Dexterity = 4, Intelligence = 2 };
            warrior.Levelup(1);
            PrimaryAttributes actual = warrior.BaseAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterLevelUpRogue_CheckLevelStats_ShouldBeAnStatIncrease()
        {
            Rogue rogue = new Rogue("rogue");
            PrimaryAttributes expected = new() { Strength = 3, Dexterity = 10, Intelligence = 2 };
            rogue.Levelup(1);
            PrimaryAttributes actual = rogue.BaseAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterLevelUpRanger_CheckLevelStats_ShouldBeAnStatIncrease()
        {
            Ranger ranger = new Ranger("ranger");
            PrimaryAttributes expected = new() { Strength = 2, Dexterity = 12, Intelligence = 2 };
            ranger.Levelup(1);
            PrimaryAttributes actual = ranger.BaseAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterLevelUpMage_CheckLevelStats_ShouldBeAnStatIncrease()
        {
            Mage mage = new Mage("mage");
            PrimaryAttributes expected = new() { Strength = 2, Dexterity = 2, Intelligence = 13 };
            mage.Levelup(1);
            PrimaryAttributes actual = mage.BaseAttributes;

            Assert.Equal(expected, actual);
        }

    }
}
